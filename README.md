Ansible Playbook:\
install puppet agent 5 on Oracle Linux
======================================

quick and dirty ansible playbook to install puppet agent 5 on oracle
linux. It is tested on oracle Linux 7 and *should* work on the RedHat
family.

#### CVS:

https://gitlab.com/e.heim/install\_puppet\_agent.git

#### steps

Installs the latest puppet 5 agent and starts/enables the puppet
service.

Sets runinterval to 30 min

Sets puppet certname to the inventory hostname

Initiates the first run

dependencies
------------

Ansible &gt; 2.1

ssh access to the hosts

user with elevated rights

install
-------

-   clone the repo to a folder

-   cd into the folder

setup
-----

#### provide **hostfile** with:

>     <fqdn>  # also used to create the certificate name within the puppet.conf
>
>     [all:vars]
>     ansible_port=22    
>     ansible_user=<name>
>     ansible_ssh_private_key_file=<path to key file>

#### **change the playbook** (install\_puppet\_agent.yml) 

provide, if necessary, a https/https proxy within the tasks:

>     - name: install puppetlabs repo from https repo
>     - name: install puppet agent (latest)

#### provide the FQDN of the puppet server to connect to

>     - name: create file with content example
>       [...] 
>       content: | 
>         [main] 
>         server = <puppet server FQND> 
>         certname = {{ inventory_hostname }} 
>         [agent] 

run
---

with: ansible-playbook -i hosts install\_puppet\_agent.yml
